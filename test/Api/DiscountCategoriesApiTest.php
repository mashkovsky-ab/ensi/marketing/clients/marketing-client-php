<?php
/**
 * DiscountCategoriesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\MarketingClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. Marketing.
 *
 * Маркетинг
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\MarketingClient;

use \Ensi\MarketingClient\Configuration;
use \Ensi\MarketingClient\ApiException;
use \Ensi\MarketingClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * DiscountCategoriesApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\MarketingClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class DiscountCategoriesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createDiscountCategory
     *
     * Создание объекта типа DiscountCategory.
     *
     */
    public function testCreateDiscountCategory()
    {
    }

    /**
     * Test case for deleteDiscountCategory
     *
     * Удаление объекта типа DiscountCategory.
     *
     */
    public function testDeleteDiscountCategory()
    {
    }

    /**
     * Test case for getDiscountCategory
     *
     * Получение объекта типа DiscountCategory.
     *
     */
    public function testGetDiscountCategory()
    {
    }

    /**
     * Test case for patchDiscountCategory
     *
     * Обновления части полей объекта типа DiscountCategory.
     *
     */
    public function testPatchDiscountCategory()
    {
    }

    /**
     * Test case for searchDiscountCategories
     *
     * Поиск объектов типа DiscountCategory.
     *
     */
    public function testSearchDiscountCategories()
    {
    }
}
