<?php
/**
 * DiscountExternalTypeEnumTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\MarketingClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. Marketing.
 *
 * Маркетинг
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\MarketingClient;

use PHPUnit\Framework\TestCase;

/**
 * DiscountExternalTypeEnumTest Class Doc Comment
 *
 * @category    Class
 * @description Типы скидок в чекауте (для пользователя):   * &#x60;1&#x60; - скидка \&quot;На товар\&quot;   * &#x60;2&#x60; - скидка \&quot;На доставку\&quot;   * &#x60;3&#x60; - скидка \&quot;На корзину\&quot;   * &#x60;4&#x60; - скидка \&quot;Для Вас\&quot;   * &#x60;5&#x60; - скидка \&quot;По промокоду\&quot;
 * @package     Ensi\MarketingClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class DiscountExternalTypeEnumTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "DiscountExternalTypeEnum"
     */
    public function testDiscountExternalTypeEnum()
    {
    }
}
