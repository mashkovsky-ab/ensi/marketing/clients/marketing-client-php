# # OffersDiscountCheckoutResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | Идентификатор оффера | [optional] 
**price** | **float** | Цена оффера с учетом примененных скидок | [optional] 
**cost** | **float** | Цена оффера с без учета примененных скидок | [optional] 
**change** | **float** | Суммарная скидка (в рублях) для данного оффера | [optional] 
**discounts** | [**\Ensi\MarketingClient\Dto\OffersDiscountCatalogResponseDiscounts[]**](OffersDiscountCatalogResponseDiscounts.md) |  | [optional] 
**qty** | **float** | Количество оффера | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


