# # PromoCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**type** | **int** | Тип промокода из PromoCodeTypeEnum | 
**creator_id** | **int** | ID пользователя (создателя) | 
**owner_id** | **int** | ID владельца промокода | [optional] 
**seller_id** | **int** | ID продавца | [optional] 
**status** | **int** | статус промокода из PromoCodeStatusEnum | 
**name** | **string** | Название промокода | 
**code** | **string** | Код | 
**counter** | **int** | Ограничение на количество использований промокода | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия промокода | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия промокода | [optional] 
**discount_id** | **int** | ID скидки | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания промокода | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


