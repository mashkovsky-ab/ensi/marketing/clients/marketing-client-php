# # DiscountOfferCatalogRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | Идентификатор оффера | 
**price** | **float** | Цена оффера | 
**product_id** | **int** | Идентификатор товара | 
**category_id** | **int** | Идентификатор категории | 
**brand_id** | **int** | Идентификатор бренда | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


