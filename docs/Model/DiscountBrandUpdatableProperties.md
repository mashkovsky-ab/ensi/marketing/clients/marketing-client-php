# # DiscountBrandUpdatableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_id** | **int** | Идентификатор бренда | [optional] 
**except** | **bool** | Является ли это правило исключением | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


