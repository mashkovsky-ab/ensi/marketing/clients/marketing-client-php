# # CalculateForCheckoutResponseDataPromoCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор промокода | [optional] 
**type** | **int** | Тип промокода из PromoCodeTypeEnum | [optional] 
**code** | **string** | Код | [optional] 
**is_personal** | **bool** | является ли промокод индивидуальным | [optional] 
**change** | **float** | итоговая скидка по промокоду | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


