# # RequestBodyPagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cursor** | **string** |  | [optional] 
**limit** | **int** |  | [optional] 
**type** | [**\Ensi\MarketingClient\Dto\PaginationTypeEnum**](PaginationTypeEnum.md) |  | [optional] 
**offset** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


