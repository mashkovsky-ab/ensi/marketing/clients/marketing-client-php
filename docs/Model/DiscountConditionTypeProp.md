# # DiscountConditionTypeProp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID типа ограничения предоставления скидки из DiscountConditionTypePropEnum | [optional] 
**name** | **string** | Название типа ограничения предоставления скидки | [optional] 
**type** | **string** | Тип данных | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


