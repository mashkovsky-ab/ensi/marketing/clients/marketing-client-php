# # CalculateForCheckoutResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promo_code** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutResponseDataPromoCode**](CalculateForCheckoutResponseDataPromoCode.md) |  | [optional] 
**discounts** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutResponseDataDiscounts[]**](CalculateForCheckoutResponseDataDiscounts.md) |  | [optional] 
**offers** | [**\Ensi\MarketingClient\Dto\OffersDiscountCheckoutResponse[]**](OffersDiscountCheckoutResponse.md) |  | [optional] 
**deliveries** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutResponseDataDeliveries[]**](CalculateForCheckoutResponseDataDeliveries.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


