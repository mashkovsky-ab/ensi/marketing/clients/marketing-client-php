# # DiscountCondition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**discount_id** | **int** | Идентификатор скидки | 
**type** | **int** | Тип условия предоставления скидки из DiscountConditionTypeEnum | 
**condition** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


