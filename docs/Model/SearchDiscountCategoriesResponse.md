# # SearchDiscountCategoriesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\MarketingClient\Dto\DiscountCategory[]**](DiscountCategory.md) |  | 
**meta** | [**\Ensi\MarketingClient\Dto\SearchDiscountsResponseMeta**](SearchDiscountsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


