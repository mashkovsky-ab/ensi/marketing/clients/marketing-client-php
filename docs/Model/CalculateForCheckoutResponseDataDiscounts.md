# # CalculateForCheckoutResponseDataDiscounts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор скидки | [optional] 
**name** | **string** | Наименование скидки | [optional] 
**type** | **int** | Тип скидки из DiscountTypeEnum | [optional] 
**external_type** | **int** | Внешний тип скидки из DiscountExternalTypeEnum | [optional] 
**change** | **float** | Итоговый размер скидки (в рублях) | [optional] 
**seller_id** | **int** | Идентификатор продавца | [optional] 
**visible_in_catalog** | **bool** | Видна ли данная скидка в каталоге | [optional] 
**promo_code_only** | **bool** | Скидка действительно только по промокоду | [optional] 
**promo_code** | **string** | Промокод | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


