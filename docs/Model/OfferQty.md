# # OfferQty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qty** | **float** | Количество оффера | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


