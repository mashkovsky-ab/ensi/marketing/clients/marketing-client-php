# # DiscountOffer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**discount_id** | **int** | Идентификатор скидки | 
**offer_id** | **int** | Идентификатор оффера | 
**except** | **bool** | Является ли это правило исключением | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


