# # CalculateForCheckoutResponseDataDeliveries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **int** | Идентификатор метода доставки | [optional] 
**price** | **float** | Цена доставки с учетом всех скидок | [optional] 
**cost** | **float** | Стоимость доставки без учета скидок | [optional] 
**selected** | **bool** | Выбран ли данный метод досавки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


