# # CalculateForCatalogRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\Ensi\MarketingClient\Dto\DiscountOfferCatalogRequest[]**](DiscountOfferCatalogRequest.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


