# # DiscountConditionRaw

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_id** | **int** | Идентификатор скидки | [optional] 
**type** | **int** | Тип условия предоставления скидки из DiscountConditionTypeEnum | [optional] 
**condition** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


