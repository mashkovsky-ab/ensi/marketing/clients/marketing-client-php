# # DiscountCategoryRaw

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_id** | **int** | Идентификатор скидки | [optional] 
**category_id** | **int** | Идентификатор категории | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


