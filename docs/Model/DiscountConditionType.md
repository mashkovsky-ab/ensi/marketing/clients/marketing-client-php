# # DiscountConditionType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID типа условия предоставления скидки из DiscountConditionTypeEnum | [optional] 
**name** | **string** | Название типа условия предоставления скидки | [optional] 
**props** | **string[]** | Обязательные поля для заполнения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


