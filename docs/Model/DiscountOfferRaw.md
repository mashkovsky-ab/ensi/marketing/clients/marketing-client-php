# # DiscountOfferRaw

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_id** | **int** | Идентификатор скидки | [optional] 
**offer_id** | **int** | Идентификатор оффера | [optional] 
**except** | **bool** | Является ли это правило исключением | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


