# # OffersDiscountCatalogResponseDiscounts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор скидки | [optional] 
**change** | **float** | Изменение цены в рублях | [optional] 
**value** | **float** | Размер скидки (в рублях или процентах) | [optional] 
**value_type** | **int** | Тип значения скидки из DiscountValueTypeEnum | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


