# # ResponseBodyPagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cursor** | **string** |  | [optional] 
**limit** | **int** |  | [optional] 
**next_cursor** | **string** |  | [optional] 
**previous_cursor** | **string** |  | [optional] 
**type** | [**\Ensi\MarketingClient\Dto\PaginationTypeEnum**](PaginationTypeEnum.md) |  | [optional] 
**offset** | **int** |  | [optional] 
**total** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


