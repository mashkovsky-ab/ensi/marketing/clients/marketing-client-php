# # DiscountBrandRaw

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_id** | **int** | Идентификатор скидки | [optional] 
**brand_id** | **int** | Идентификатор бренда | [optional] 
**except** | **bool** | Является ли это правило исключением | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


