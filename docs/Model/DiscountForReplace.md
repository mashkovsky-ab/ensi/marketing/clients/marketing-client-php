# # DiscountForReplace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**seller_id** | **int** | Идентификатор продавца | [optional] 
**name** | **string** | Наименование скидки | [optional] 
**value_type** | **int** | Тип значения скидки из DiscountValueTypeEnum | [optional] 
**value** | **float** | Размер скидки | [optional] 
**status** | **int** | Статус скидки из DiscountStatusEnum | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия скидки | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия скидки | [optional] 
**promo_code_only** | **bool** | Скидка действительна только по промокоду | [optional] 
**offers** | [**\Ensi\MarketingClient\Dto\DiscountOffer[]**](DiscountOffer.md) |  | [optional] 
**brands** | [**\Ensi\MarketingClient\Dto\DiscountBrand[]**](DiscountBrand.md) |  | [optional] 
**categories** | [**\Ensi\MarketingClient\Dto\DiscountCategory[]**](DiscountCategory.md) |  | [optional] 
**segments** | [**\Ensi\MarketingClient\Dto\DiscountSegment[]**](DiscountSegment.md) |  | [optional] 
**conditions** | [**\Ensi\MarketingClient\Dto\DiscountCondition[]**](DiscountCondition.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


