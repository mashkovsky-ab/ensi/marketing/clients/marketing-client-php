# # CalculateForCheckoutRequestPayment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **int** | Идентификатор метода оплаты | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


