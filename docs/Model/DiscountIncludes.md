# # DiscountIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\Ensi\MarketingClient\Dto\DiscountOffer[]**](DiscountOffer.md) |  | [optional] 
**brands** | [**\Ensi\MarketingClient\Dto\DiscountBrand[]**](DiscountBrand.md) |  | [optional] 
**categories** | [**\Ensi\MarketingClient\Dto\DiscountCategory[]**](DiscountCategory.md) |  | [optional] 
**segments** | [**\Ensi\MarketingClient\Dto\DiscountSegment[]**](DiscountSegment.md) |  | [optional] 
**conditions** | [**\Ensi\MarketingClient\Dto\DiscountCondition[]**](DiscountCondition.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


