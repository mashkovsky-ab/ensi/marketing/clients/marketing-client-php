# # DiscountBrand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**discount_id** | **int** | Идентификатор скидки | 
**brand_id** | **int** | Идентификатор бренда | 
**except** | **bool** | Является ли это правило исключением | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


