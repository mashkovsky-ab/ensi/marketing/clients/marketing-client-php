# # CalculateForCheckoutRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\Ensi\MarketingClient\Dto\DiscountOfferCheckoutRequest[]**](DiscountOfferCheckoutRequest.md) |  | 
**customer** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutRequestCustomer**](CalculateForCheckoutRequestCustomer.md) |  | [optional] 
**deliveries** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutRequestDeliveries[]**](CalculateForCheckoutRequestDeliveries.md) |  | [optional] 
**payment** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutRequestPayment**](CalculateForCheckoutRequestPayment.md) |  | [optional] 
**promo_code** | **string** | Промокод | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


