# # DiscountUpdatableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**seller_id** | **int** | Идентификатор продавца | [optional] 
**name** | **string** | Наименование скидки | [optional] 
**value_type** | **int** | Тип значения скидки из DiscountValueTypeEnum | [optional] 
**value** | **float** | Размер скидки | [optional] 
**status** | **int** | Статус скидки из DiscountStatusEnum | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Дата начала действия скидки | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | Дата окончания действия скидки | [optional] 
**promo_code_only** | **bool** | Скидка действительна только по промокоду | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


