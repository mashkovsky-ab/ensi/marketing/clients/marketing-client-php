# Ensi\MarketingClient\DiscountsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculateForCatalog**](DiscountsApi.md#calculateForCatalog) | **POST** /discounts/discounts:calculate-for-catalog | Рассчет цен и скидок на офферы для отображения в каталоге
[**calculateForCheckout**](DiscountsApi.md#calculateForCheckout) | **POST** /discounts/discounts:calculate-for-checkout | Рассчет цен и скидок на офферы и доставку для отображения в чекауте
[**createDiscount**](DiscountsApi.md#createDiscount) | **POST** /discounts/discounts | Создание объекта типа Discount
[**deleteDiscount**](DiscountsApi.md#deleteDiscount) | **DELETE** /discounts/discounts/{id} | Удаление объекта типа Discount
[**getDiscount**](DiscountsApi.md#getDiscount) | **GET** /discounts/discounts/{id} | Получение объекта типа Discount
[**massDiscountsStatusUpdate**](DiscountsApi.md#massDiscountsStatusUpdate) | **POST** /discounts/discounts:mass-status-update | Массовое обновление статусов для объектов типа Discount
[**patchDiscount**](DiscountsApi.md#patchDiscount) | **PATCH** /discounts/discounts/{id} | Обновления части полей объекта типа Discount
[**replaceDiscount**](DiscountsApi.md#replaceDiscount) | **PUT** /discounts/discounts/{id} | Замена объекта типа Discount
[**searchDiscounts**](DiscountsApi.md#searchDiscounts) | **POST** /discounts/discounts:search | Поиск объектов типа Discount



## calculateForCatalog

> \Ensi\MarketingClient\Dto\CalculateForCatalogResponse calculateForCatalog($calculate_for_catalog_request)

Рассчет цен и скидок на офферы для отображения в каталоге

Рассчет цен и скидок на офферы для отображения в каталоге

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$calculate_for_catalog_request = new \Ensi\MarketingClient\Dto\CalculateForCatalogRequest(); // \Ensi\MarketingClient\Dto\CalculateForCatalogRequest | 

try {
    $result = $apiInstance->calculateForCatalog($calculate_for_catalog_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->calculateForCatalog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculate_for_catalog_request** | [**\Ensi\MarketingClient\Dto\CalculateForCatalogRequest**](../Model/CalculateForCatalogRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\CalculateForCatalogResponse**](../Model/CalculateForCatalogResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## calculateForCheckout

> \Ensi\MarketingClient\Dto\CalculateForCheckoutResponse calculateForCheckout($calculate_for_checkout_request)

Рассчет цен и скидок на офферы и доставку для отображения в чекауте

Рассчет цен и скидок на офферы и доставку для отображения в чекауте

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$calculate_for_checkout_request = new \Ensi\MarketingClient\Dto\CalculateForCheckoutRequest(); // \Ensi\MarketingClient\Dto\CalculateForCheckoutRequest | 

try {
    $result = $apiInstance->calculateForCheckout($calculate_for_checkout_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->calculateForCheckout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculate_for_checkout_request** | [**\Ensi\MarketingClient\Dto\CalculateForCheckoutRequest**](../Model/CalculateForCheckoutRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\CalculateForCheckoutResponse**](../Model/CalculateForCheckoutResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse createDiscount($discount_request)

Создание объекта типа Discount

Создание объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_request = new \Ensi\MarketingClient\Dto\DiscountRequest(); // \Ensi\MarketingClient\Dto\DiscountRequest | 

try {
    $result = $apiInstance->createDiscount($discount_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->createDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_request** | [**\Ensi\MarketingClient\Dto\DiscountRequest**](../Model/DiscountRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscount

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscount($id)

Удаление объекта типа Discount

Удаление объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscount($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->deleteDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse getDiscount($id, $include)

Получение объекта типа Discount

Получение объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDiscount($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->getDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDiscountsStatusUpdate

> \Ensi\MarketingClient\Dto\EmptyDataResponse massDiscountsStatusUpdate($mass_discounts_status_update_request)

Массовое обновление статусов для объектов типа Discount

Массовое обновление статусов для объектов типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_discounts_status_update_request = new \Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest(); // \Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest | 

try {
    $result = $apiInstance->massDiscountsStatusUpdate($mass_discounts_status_update_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->massDiscountsStatusUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_discounts_status_update_request** | [**\Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest**](../Model/MassDiscountsStatusUpdateRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse patchDiscount($id, $discount_updatable_properties)

Обновления части полей объекта типа Discount

Обновления части полей объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscount($id, $discount_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->patchDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountUpdatableProperties**](../Model/DiscountUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceDiscount

> \Ensi\MarketingClient\Dto\DiscountResponse replaceDiscount($id, $discount_for_replace)

Замена объекта типа Discount

Замена объекта типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_for_replace = new \Ensi\MarketingClient\Dto\DiscountForReplace(); // \Ensi\MarketingClient\Dto\DiscountForReplace | 

try {
    $result = $apiInstance->replaceDiscount($id, $discount_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->replaceDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_for_replace** | [**\Ensi\MarketingClient\Dto\DiscountForReplace**](../Model/DiscountForReplace.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountResponse**](../Model/DiscountResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscounts

> \Ensi\MarketingClient\Dto\SearchDiscountsResponse searchDiscounts($search_discounts_request)

Поиск объектов типа Discount

Поиск объектов типа Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discounts_request = new \Ensi\MarketingClient\Dto\SearchDiscountsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountsRequest | 

try {
    $result = $apiInstance->searchDiscounts($search_discounts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->searchDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discounts_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountsRequest**](../Model/SearchDiscountsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountsResponse**](../Model/SearchDiscountsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

