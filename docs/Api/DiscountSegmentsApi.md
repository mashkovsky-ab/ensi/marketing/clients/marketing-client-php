# Ensi\MarketingClient\DiscountSegmentsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscountSegment**](DiscountSegmentsApi.md#createDiscountSegment) | **POST** /discounts/discount-segments | Создание объекта типа DiscountSegment
[**deleteDiscountSegment**](DiscountSegmentsApi.md#deleteDiscountSegment) | **DELETE** /discounts/discount-segments/{id} | Удаление объекта типа DiscountSegment
[**getDiscountSegment**](DiscountSegmentsApi.md#getDiscountSegment) | **GET** /discounts/discount-segments/{id} | Получение объекта типа DiscountSegment
[**patchDiscountSegment**](DiscountSegmentsApi.md#patchDiscountSegment) | **PATCH** /discounts/discount-segments/{id} | Обновления части полей объекта типа DiscountSegment
[**searchDiscountSegments**](DiscountSegmentsApi.md#searchDiscountSegments) | **POST** /discounts/discount-segments:search | Поиск объектов типа DiscountSegment



## createDiscountSegment

> \Ensi\MarketingClient\Dto\DiscountSegmentResponse createDiscountSegment($discount_segment_raw)

Создание объекта типа DiscountSegment

Создание объекта типа DiscountSegment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountSegmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_segment_raw = new \Ensi\MarketingClient\Dto\DiscountSegmentRaw(); // \Ensi\MarketingClient\Dto\DiscountSegmentRaw | 

try {
    $result = $apiInstance->createDiscountSegment($discount_segment_raw);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountSegmentsApi->createDiscountSegment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_segment_raw** | [**\Ensi\MarketingClient\Dto\DiscountSegmentRaw**](../Model/DiscountSegmentRaw.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountSegmentResponse**](../Model/DiscountSegmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscountSegment

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountSegment($id)

Удаление объекта типа DiscountSegment

Удаление объекта типа DiscountSegment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountSegmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscountSegment($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountSegmentsApi->deleteDiscountSegment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountSegment

> \Ensi\MarketingClient\Dto\DiscountSegmentResponse getDiscountSegment($id)

Получение объекта типа DiscountSegment

Получение объекта типа DiscountSegment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountSegmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountSegment($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountSegmentsApi->getDiscountSegment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountSegmentResponse**](../Model/DiscountSegmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountSegment

> \Ensi\MarketingClient\Dto\DiscountSegmentResponse patchDiscountSegment($id, $discount_segment_updatable_properties)

Обновления части полей объекта типа DiscountSegment

Обновления части полей объекта типа DiscountSegment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountSegmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_segment_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountSegmentUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountSegmentUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscountSegment($id, $discount_segment_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountSegmentsApi->patchDiscountSegment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_segment_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountSegmentUpdatableProperties**](../Model/DiscountSegmentUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountSegmentResponse**](../Model/DiscountSegmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountSegments

> \Ensi\MarketingClient\Dto\SearchDiscountSegmentsResponse searchDiscountSegments($search_discount_segments_request)

Поиск объектов типа DiscountSegment

Поиск объектов типа DiscountSegment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountSegmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_segments_request = new \Ensi\MarketingClient\Dto\SearchDiscountSegmentsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountSegmentsRequest | 

try {
    $result = $apiInstance->searchDiscountSegments($search_discount_segments_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountSegmentsApi->searchDiscountSegments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_segments_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountSegmentsRequest**](../Model/SearchDiscountSegmentsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountSegmentsResponse**](../Model/SearchDiscountSegmentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

