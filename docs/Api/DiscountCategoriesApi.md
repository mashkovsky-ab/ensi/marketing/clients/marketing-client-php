# Ensi\MarketingClient\DiscountCategoriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscountCategory**](DiscountCategoriesApi.md#createDiscountCategory) | **POST** /discounts/discount-categories | Создание объекта типа DiscountCategory
[**deleteDiscountCategory**](DiscountCategoriesApi.md#deleteDiscountCategory) | **DELETE** /discounts/discount-categories/{id} | Удаление объекта типа DiscountCategory
[**getDiscountCategory**](DiscountCategoriesApi.md#getDiscountCategory) | **GET** /discounts/discount-categories/{id} | Получение объекта типа DiscountCategory
[**patchDiscountCategory**](DiscountCategoriesApi.md#patchDiscountCategory) | **PATCH** /discounts/discount-categories/{id} | Обновления части полей объекта типа DiscountCategory
[**searchDiscountCategories**](DiscountCategoriesApi.md#searchDiscountCategories) | **POST** /discounts/discount-categories:search | Поиск объектов типа DiscountCategory



## createDiscountCategory

> \Ensi\MarketingClient\Dto\DiscountCategoryResponse createDiscountCategory($discount_category_raw)

Создание объекта типа DiscountCategory

Создание объекта типа DiscountCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_category_raw = new \Ensi\MarketingClient\Dto\DiscountCategoryRaw(); // \Ensi\MarketingClient\Dto\DiscountCategoryRaw | 

try {
    $result = $apiInstance->createDiscountCategory($discount_category_raw);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountCategoriesApi->createDiscountCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_category_raw** | [**\Ensi\MarketingClient\Dto\DiscountCategoryRaw**](../Model/DiscountCategoryRaw.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountCategoryResponse**](../Model/DiscountCategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscountCategory

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountCategory($id)

Удаление объекта типа DiscountCategory

Удаление объекта типа DiscountCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscountCategory($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountCategoriesApi->deleteDiscountCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountCategory

> \Ensi\MarketingClient\Dto\DiscountCategoryResponse getDiscountCategory($id)

Получение объекта типа DiscountCategory

Получение объекта типа DiscountCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountCategory($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountCategoriesApi->getDiscountCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountCategoryResponse**](../Model/DiscountCategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountCategory

> \Ensi\MarketingClient\Dto\DiscountCategoryResponse patchDiscountCategory($id, $discount_category_updatable_properties)

Обновления части полей объекта типа DiscountCategory

Обновления части полей объекта типа DiscountCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_category_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountCategoryUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountCategoryUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscountCategory($id, $discount_category_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountCategoriesApi->patchDiscountCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_category_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountCategoryUpdatableProperties**](../Model/DiscountCategoryUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountCategoryResponse**](../Model/DiscountCategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountCategories

> \Ensi\MarketingClient\Dto\SearchDiscountCategoriesResponse searchDiscountCategories($search_discount_categories_request)

Поиск объектов типа DiscountCategory

Поиск объектов типа DiscountCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_categories_request = new \Ensi\MarketingClient\Dto\SearchDiscountCategoriesRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountCategoriesRequest | 

try {
    $result = $apiInstance->searchDiscountCategories($search_discount_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountCategoriesApi->searchDiscountCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_categories_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountCategoriesRequest**](../Model/SearchDiscountCategoriesRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountCategoriesResponse**](../Model/SearchDiscountCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

