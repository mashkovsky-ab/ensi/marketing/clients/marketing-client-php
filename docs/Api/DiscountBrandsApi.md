# Ensi\MarketingClient\DiscountBrandsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscountBrand**](DiscountBrandsApi.md#createDiscountBrand) | **POST** /discounts/discount-brands | Создание объекта типа DiscountBrand
[**deleteDiscountBrand**](DiscountBrandsApi.md#deleteDiscountBrand) | **DELETE** /discounts/discount-brands/{id} | Удаление объекта типа DiscountBrand
[**getDiscountBrand**](DiscountBrandsApi.md#getDiscountBrand) | **GET** /discounts/discount-brands/{id} | Получение объекта типа DiscountBrand
[**patchDiscountBrand**](DiscountBrandsApi.md#patchDiscountBrand) | **PATCH** /discounts/discount-brands/{id} | Обновления части полей объекта типа DiscountBrand
[**searchDiscountBrands**](DiscountBrandsApi.md#searchDiscountBrands) | **POST** /discounts/discount-brands:search | Поиск объектов типа DiscountBrand



## createDiscountBrand

> \Ensi\MarketingClient\Dto\DiscountBrandResponse createDiscountBrand($discount_brand_raw)

Создание объекта типа DiscountBrand

Создание объекта типа DiscountBrand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountBrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_brand_raw = new \Ensi\MarketingClient\Dto\DiscountBrandRaw(); // \Ensi\MarketingClient\Dto\DiscountBrandRaw | 

try {
    $result = $apiInstance->createDiscountBrand($discount_brand_raw);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountBrandsApi->createDiscountBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_brand_raw** | [**\Ensi\MarketingClient\Dto\DiscountBrandRaw**](../Model/DiscountBrandRaw.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountBrandResponse**](../Model/DiscountBrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscountBrand

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountBrand($id)

Удаление объекта типа DiscountBrand

Удаление объекта типа DiscountBrand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountBrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscountBrand($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountBrandsApi->deleteDiscountBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountBrand

> \Ensi\MarketingClient\Dto\DiscountBrandResponse getDiscountBrand($id)

Получение объекта типа DiscountBrand

Получение объекта типа DiscountBrand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountBrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountBrand($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountBrandsApi->getDiscountBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountBrandResponse**](../Model/DiscountBrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountBrand

> \Ensi\MarketingClient\Dto\DiscountBrandResponse patchDiscountBrand($id, $discount_brand_updatable_properties)

Обновления части полей объекта типа DiscountBrand

Обновления части полей объекта типа DiscountBrand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountBrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_brand_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountBrandUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountBrandUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscountBrand($id, $discount_brand_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountBrandsApi->patchDiscountBrand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_brand_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountBrandUpdatableProperties**](../Model/DiscountBrandUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountBrandResponse**](../Model/DiscountBrandResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountBrands

> \Ensi\MarketingClient\Dto\SearchDiscountBrandsResponse searchDiscountBrands($search_discount_brands_request)

Поиск объектов типа DiscountBrand

Поиск объектов типа DiscountBrand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountBrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_brands_request = new \Ensi\MarketingClient\Dto\SearchDiscountBrandsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountBrandsRequest | 

try {
    $result = $apiInstance->searchDiscountBrands($search_discount_brands_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountBrandsApi->searchDiscountBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_brands_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountBrandsRequest**](../Model/SearchDiscountBrandsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountBrandsResponse**](../Model/SearchDiscountBrandsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

