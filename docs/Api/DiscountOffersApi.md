# Ensi\MarketingClient\DiscountOffersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscountOffer**](DiscountOffersApi.md#createDiscountOffer) | **POST** /discounts/discount-offers | Создание объекта типа DiscountOffer
[**deleteDiscountOffer**](DiscountOffersApi.md#deleteDiscountOffer) | **DELETE** /discounts/discount-offers/{id} | Удаление объекта типа DiscountOffer
[**getDiscountOffer**](DiscountOffersApi.md#getDiscountOffer) | **GET** /discounts/discount-offers/{id} | Получение объекта типа DiscountOffer
[**patchDiscountOffer**](DiscountOffersApi.md#patchDiscountOffer) | **PATCH** /discounts/discount-offers/{id} | Обновления части полей объекта типа DiscountOffer
[**searchDiscountOffers**](DiscountOffersApi.md#searchDiscountOffers) | **POST** /discounts/discount-offers:search | Поиск объектов типа DiscountOffer



## createDiscountOffer

> \Ensi\MarketingClient\Dto\DiscountOfferResponse createDiscountOffer($discount_offer_raw)

Создание объекта типа DiscountOffer

Создание объекта типа DiscountOffer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_offer_raw = new \Ensi\MarketingClient\Dto\DiscountOfferRaw(); // \Ensi\MarketingClient\Dto\DiscountOfferRaw | 

try {
    $result = $apiInstance->createDiscountOffer($discount_offer_raw);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOffersApi->createDiscountOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_offer_raw** | [**\Ensi\MarketingClient\Dto\DiscountOfferRaw**](../Model/DiscountOfferRaw.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountOfferResponse**](../Model/DiscountOfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscountOffer

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountOffer($id)

Удаление объекта типа DiscountOffer

Удаление объекта типа DiscountOffer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscountOffer($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOffersApi->deleteDiscountOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountOffer

> \Ensi\MarketingClient\Dto\DiscountOfferResponse getDiscountOffer($id)

Получение объекта типа DiscountOffer

Получение объекта типа DiscountOffer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountOffer($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOffersApi->getDiscountOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountOfferResponse**](../Model/DiscountOfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountOffer

> \Ensi\MarketingClient\Dto\DiscountOfferResponse patchDiscountOffer($id, $discount_offer_updatable_properties)

Обновления части полей объекта типа DiscountOffer

Обновления части полей объекта типа DiscountOffer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_offer_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountOfferUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountOfferUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscountOffer($id, $discount_offer_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOffersApi->patchDiscountOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_offer_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountOfferUpdatableProperties**](../Model/DiscountOfferUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountOfferResponse**](../Model/DiscountOfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountOffers

> \Ensi\MarketingClient\Dto\SearchDiscountOffersResponse searchDiscountOffers($search_discount_offers_request)

Поиск объектов типа DiscountOffer

Поиск объектов типа DiscountOffer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_offers_request = new \Ensi\MarketingClient\Dto\SearchDiscountOffersRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountOffersRequest | 

try {
    $result = $apiInstance->searchDiscountOffers($search_discount_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountOffersApi->searchDiscountOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_offers_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountOffersRequest**](../Model/SearchDiscountOffersRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountOffersResponse**](../Model/SearchDiscountOffersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

