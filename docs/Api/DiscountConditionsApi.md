# Ensi\MarketingClient\DiscountConditionsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscountCondition**](DiscountConditionsApi.md#createDiscountCondition) | **POST** /discounts/discount-conditions | Создание объекта типа DiscountCondition
[**deleteDiscountCondition**](DiscountConditionsApi.md#deleteDiscountCondition) | **DELETE** /discounts/discount-conditions/{id} | Удаление объекта типа DiscountCondition
[**getDiscountCondition**](DiscountConditionsApi.md#getDiscountCondition) | **GET** /discounts/discount-conditions/{id} | Получение объекта типа DiscountCondition
[**patchDiscountCondition**](DiscountConditionsApi.md#patchDiscountCondition) | **PATCH** /discounts/discount-conditions/{id} | Обновления части полей объекта типа DiscountCondition
[**searchDiscountConditions**](DiscountConditionsApi.md#searchDiscountConditions) | **POST** /discounts/discount-conditions:search | Поиск объектов типа DiscountCondition



## createDiscountCondition

> \Ensi\MarketingClient\Dto\DiscountConditionResponse createDiscountCondition($discount_condition_raw)

Создание объекта типа DiscountCondition

Создание объекта типа DiscountCondition

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountConditionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$discount_condition_raw = new \Ensi\MarketingClient\Dto\DiscountConditionRaw(); // \Ensi\MarketingClient\Dto\DiscountConditionRaw | 

try {
    $result = $apiInstance->createDiscountCondition($discount_condition_raw);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountConditionsApi->createDiscountCondition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discount_condition_raw** | [**\Ensi\MarketingClient\Dto\DiscountConditionRaw**](../Model/DiscountConditionRaw.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountConditionResponse**](../Model/DiscountConditionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDiscountCondition

> \Ensi\MarketingClient\Dto\EmptyDataResponse deleteDiscountCondition($id)

Удаление объекта типа DiscountCondition

Удаление объекта типа DiscountCondition

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountConditionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDiscountCondition($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountConditionsApi->deleteDiscountCondition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountCondition

> \Ensi\MarketingClient\Dto\DiscountConditionResponse getDiscountCondition($id)

Получение объекта типа DiscountCondition

Получение объекта типа DiscountCondition

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountConditionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getDiscountCondition($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountConditionsApi->getDiscountCondition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountConditionResponse**](../Model/DiscountConditionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDiscountCondition

> \Ensi\MarketingClient\Dto\DiscountConditionResponse patchDiscountCondition($id, $discount_condition_updatable_properties)

Обновления части полей объекта типа DiscountCondition

Обновления части полей объекта типа DiscountCondition

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountConditionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$discount_condition_updatable_properties = new \Ensi\MarketingClient\Dto\DiscountConditionUpdatableProperties(); // \Ensi\MarketingClient\Dto\DiscountConditionUpdatableProperties | 

try {
    $result = $apiInstance->patchDiscountCondition($id, $discount_condition_updatable_properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountConditionsApi->patchDiscountCondition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **discount_condition_updatable_properties** | [**\Ensi\MarketingClient\Dto\DiscountConditionUpdatableProperties**](../Model/DiscountConditionUpdatableProperties.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\DiscountConditionResponse**](../Model/DiscountConditionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDiscountConditions

> \Ensi\MarketingClient\Dto\SearchDiscountConditionsResponse searchDiscountConditions($search_discount_conditions_request)

Поиск объектов типа DiscountCondition

Поиск объектов типа DiscountCondition

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\DiscountConditionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discount_conditions_request = new \Ensi\MarketingClient\Dto\SearchDiscountConditionsRequest(); // \Ensi\MarketingClient\Dto\SearchDiscountConditionsRequest | 

try {
    $result = $apiInstance->searchDiscountConditions($search_discount_conditions_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountConditionsApi->searchDiscountConditions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discount_conditions_request** | [**\Ensi\MarketingClient\Dto\SearchDiscountConditionsRequest**](../Model/SearchDiscountConditionsRequest.md)|  |

### Return type

[**\Ensi\MarketingClient\Dto\SearchDiscountConditionsResponse**](../Model/SearchDiscountConditionsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

