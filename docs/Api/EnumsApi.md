# Ensi\MarketingClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDiscountConditionTypeProps**](EnumsApi.md#getDiscountConditionTypeProps) | **GET** /discounts/discount-condition-type-props | Получение объектов типа DiscountConditionTypeProp
[**getDiscountConditionTypes**](EnumsApi.md#getDiscountConditionTypes) | **GET** /discounts/discount-condition-types | Получение объектов типа DiscountConditionType
[**getDiscountStatuses**](EnumsApi.md#getDiscountStatuses) | **GET** /discounts/discount-statuses | Получение объектов типа DiscountStatus
[**getDiscountTypes**](EnumsApi.md#getDiscountTypes) | **GET** /discounts/discount-types | Получение объектов типа DiscountType
[**getPromoCodeStatuses**](EnumsApi.md#getPromoCodeStatuses) | **GET** /promo-codes/promo-code-statuses | Получение объектов типа PromoCodeStatus
[**getPromoCodeTypes**](EnumsApi.md#getPromoCodeTypes) | **GET** /promo-codes/promo-code-types | Получение объектов типа PromoCodeType



## getDiscountConditionTypeProps

> \Ensi\MarketingClient\Dto\GetDiscountConditionTypePropsResponse getDiscountConditionTypeProps()

Получение объектов типа DiscountConditionTypeProp

Получение объектов типа DiscountConditionTypeProp

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDiscountConditionTypeProps();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getDiscountConditionTypeProps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetDiscountConditionTypePropsResponse**](../Model/GetDiscountConditionTypePropsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountConditionTypes

> \Ensi\MarketingClient\Dto\GetDiscountConditionTypesResponse getDiscountConditionTypes()

Получение объектов типа DiscountConditionType

Получение объектов типа DiscountConditionType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDiscountConditionTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getDiscountConditionTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetDiscountConditionTypesResponse**](../Model/GetDiscountConditionTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountStatuses

> \Ensi\MarketingClient\Dto\GetDiscountStatusesResponse getDiscountStatuses()

Получение объектов типа DiscountStatus

Получение объектов типа DiscountStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDiscountStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getDiscountStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetDiscountStatusesResponse**](../Model/GetDiscountStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDiscountTypes

> \Ensi\MarketingClient\Dto\GetDiscountTypesResponse getDiscountTypes()

Получение объектов типа DiscountType

Получение объектов типа DiscountType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDiscountTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getDiscountTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetDiscountTypesResponse**](../Model/GetDiscountTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPromoCodeStatuses

> \Ensi\MarketingClient\Dto\GetPromoCodeStatusesResponse getPromoCodeStatuses()

Получение объектов типа PromoCodeStatus

Получение объектов типа PromoCodeStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPromoCodeStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPromoCodeStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetPromoCodeStatusesResponse**](../Model/GetPromoCodeStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPromoCodeTypes

> \Ensi\MarketingClient\Dto\GetPromoCodeTypesResponse getPromoCodeTypes()

Получение объектов типа PromoCodeType

Получение объектов типа PromoCodeType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\MarketingClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPromoCodeTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPromoCodeTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\MarketingClient\Dto\GetPromoCodeTypesResponse**](../Model/GetPromoCodeTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

